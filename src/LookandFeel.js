import React from 'react';
import './App.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import Carousel from 'react-bootstrap/Carousel';
import Pagination from 'react-bootstrap/Pagination';
import Alert from 'react-bootstrap/Alert';

 
function LookandFeel(props) {
     return(
         <div>
           <Container  fluid>
             <head>
             <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous" />
             </head>
             <Row>
             <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                <a className="navbar-brand" href="#">E-Commerce</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="login.html">Login</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Register</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Shop</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Profile</a>
                    </li>
                    </ul>
                </div>
                </div>
            </nav>
            </Row>
            
            <Row>
            
            <Col xs={3}>
            <h2>Card</h2>
            <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="holder.js/100px180?text=Image cap" />
            <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
        Some quick example text to build on the card title and make up the bulk of
        the card's content.
          </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>Cras justo odio</ListGroupItem>
          <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
          <ListGroupItem>Vestibulum at eros</ListGroupItem>
        </ListGroup>
        <Card.Body>
          <Card.Link href="#" className="link">Card Link</Card.Link>
          <Card.Link href="#" className="link">Another Link</Card.Link>
        </Card.Body>
        <Button variant="outline-secondary">Go somewhere</Button>{' '}
        <Button variant="outline-secondary" size="sm" >Edit profile</Button>
      </Card>
      </Col>
      <Col xs={5}>
      <h3>Table</h3>
                    <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                            <h3>Badges</h3>
                            <h4>                             
                              <Badge variant="secondary">New</Badge>{' '}
                              <Badge variant="success">Success</Badge>{' '}
                              <Badge variant="danger">Danger</Badge>{' '}
                              <Badge variant="warning">Warning</Badge>{' '}
                              <Badge variant="info">Info</Badge>{' '}
                              <Badge variant="dark">Dark</Badge>
                            </h4>
                            <p/>
                            <h3>Buttons</h3>
                            <Button variant="primary" >edit product</Button>{' '}
                            <Button variant="secondary" >edit product</Button>{' '}
                            <Button variant="outline-secondary" >Add product</Button>{' '}
                            <Button variant="outline-secondary" >edit product</Button>
                            <p/>
                            <h3>Pagination</h3>
                            <Pagination>
                              <Pagination.First />
                              <Pagination.Item>{1}</Pagination.Item>
                              <Pagination.Ellipsis />

                              <Pagination.Item>{10}</Pagination.Item>
                              <Pagination.Item>{11}</Pagination.Item>
                              <Pagination.Item active>{12}</Pagination.Item>
                              <Pagination.Ellipsis />
                              <Pagination.Item>{20}</Pagination.Item>
                              <Pagination.Last />
                            </Pagination>
                  </Col>
                  <Col >
                  <h3>Carousel</h3>
                  <Carousel>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="https://www.sucatroca.com.br/site/src/images/placeholders/180x180.gif"
                      alt="First slide"
                    />
                    <Carousel.Caption>
                      <h3>First slide label</h3>
                      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="https://civilrights.msu.edu/_assets/images/placeholder/placeholder-200x200.jpg"
                      alt="Third slide"
                    />

                    <Carousel.Caption>
                      <h3>Second slide label</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="https://www.dacorumaerials.com/wp-content/uploads/2017/11/default-placeholder-180x180.png"
                      alt="Third slide"
                    />

                    <Carousel.Caption>
                      <h3>Third slide label</h3>
                      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                </Carousel>
                  </Col>
              </Row>
              <Row>
                  <Col xs={3}>
                  <br/>
                  <h2>Tabs</h2>
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                      <Tab eventKey="About" title="About" >
                        <ListGroup>
                            <ListGroup.Item>User Id</ListGroup.Item>
                            <ListGroup.Item>Name</ListGroup.Item>
                            <ListGroup.Item>Email</ListGroup.Item>
                            <ListGroup.Item>Phone</ListGroup.Item>
                          </ListGroup>
                      </Tab>
                      <Tab eventKey="home" title="Home">
                      stuff
                      </Tab>
                      <Tab eventKey="Purchase" title="Purchase">
                      stuff
                      </Tab>
                    </Tabs>
                    </Col>
                    <Col xs={5}>
                    <br/>
                    <h2>Form</h2>
                    <Form>
                        <Form.Row className="align-items-center">
                            <Form.Group as={Col} controlId="1">
                            <Form.Label>Type of product</Form.Label>
                            <Form.Control as="select" defaultValue="Choose...">
                            <option>Choose...</option>
                            <option>...</option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridZip">
                            <Form.Label>Product ID</Form.Label>
                            <Form.Control placeholder="Product"/>
                            </Form.Group>
                            <Form.Group as={Col} controlId="2">
                            <br/>
                            <Button variant="outline-secondary"  >Search</Button>
                            </Form.Group>
                            <Form.Group as={Col} controlId="2">
                            </Form.Group>
                        </Form.Row>
                        
                    </Form>
                    </Col>
                    <Col>
                    <br/>
                    <h3>Alerts</h3>
                        <Alert key={1} variant='primary'>
                          This is an alert—check it out!
                        </Alert>
                        <Alert key={2} variant='secondary'>
                          This is an alert—check it out!
                        </Alert>
                        <Alert key={3} variant='success'>
                          This is an alert—check it out!
                        </Alert>
                        <Alert key={4} variant='danger'>
                          This is an alert—check it out!
                        </Alert>
                        <Alert key={5} variant='warning'>
                          This is an alert—check it out!
                        </Alert>
                    </Col>
            </Row>
            <Row>
              <Col xs={3}>
              <h3>Modal</h3>
              <Modal.Dialog>
              <Modal.Header closeButton>
                <Modal.Title>Modal title</Modal.Title>
              </Modal.Header>

              <Modal.Body>
                <p>Modal body text goes here.</p>
              </Modal.Body>

              <Modal.Footer>
                <Button variant="secondary">Close</Button>
                <Button variant="outline-secondary">Save changes</Button>
              </Modal.Footer>
            </Modal.Dialog>              
              </Col>
              <Col>
               <h2>Fonts</h2>
               <h1>Iconos</h1>
               <i className="far fa-user fa-5x"></i>
               <i className="fas fa-search fa-5x"></i>
               <i className="fas fa-shopping-bag fa-5x"></i>
               <i className="fab fa-instagram fa-5x"></i>


              </Col>
            </Row>
            <br/>
            <h2>Footer</h2>
            <footer className="py-5 bg-dark">
                <div className="container">
                <p className="m-0 text-center text-white">Copyright &copy; PK 2020</p>
                </div>

            </footer>
          
          </Container>
          
         </div>
     )
 }

 export default LookandFeel;