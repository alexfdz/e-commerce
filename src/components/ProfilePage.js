import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Card from 'react-bootstrap/Card';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import Table from 'react-bootstrap/Table';
//import Nav from 'react-bootstrap/Nav';

 function  ProfilePage(props) {
        return(
        <div>
            <Container fluid>
                <Row>
                    <Col xs={4}>

                    </Col>
                    <Col xs={6}>
                     <h2>Profile page</h2> 
                    </Col>
                    <Col>
                        <br/>
                        <Button variant="outline-secondary" size="sm" >Edit profile</Button>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4}>
                        <Row className="justify-content-md-center">
                        <Image src="http://www.ub.edu/idp/web/sites/default/files/staff/2019-10/default-avatar.png" fluid/>
                        </Row>
                    <Card>
                    <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="outline-secondary">Go somewhere</Button>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                    <ListGroupItem>Orders</ListGroupItem>
                    <ListGroupItem>Payments</ListGroupItem>
                    <ListGroupItem>Wishlist</ListGroupItem>
                    </ListGroup>
                    </Card>
                    </Col>
                    <Col xs={8}>
                        <br/>
                        <h2>Username</h2> 
                        <h4>Total Purchases: 3</h4>
                        <br/>
                        <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" variant="tabs">
                        <Tab eventKey="home" title="About" >
                            <ListGroup>
                                <ListGroup.Item>User Id</ListGroup.Item>
                                <ListGroup.Item>Name</ListGroup.Item>
                                <ListGroup.Item>Email</ListGroup.Item>
                                <ListGroup.Item>Phone</ListGroup.Item>
                            </ListGroup>
                        </Tab>
                        <Tab eventKey="profile" title="Purchase info">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Cant.</th>
                                <th>Price</th>
                                <th>Total cost</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>clothes</td>
                                    <td>2</td>
                                    <td>19.99</td>
                                    <td>39.98</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>clothes</td>
                                    <td>2</td>
                                    <td>19.99</td>
                                    <td>39.98</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>clothes</td>
                                    <td>2</td>
                                    <td>19.99</td>
                                    <td>39.98</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        </Tabs>
                    </Col>
                </Row>
            </Container>
        </div>
        );
    
}
export default ProfilePage;
/*

                    <Nav
                    activeKey="/home"
                    onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
                    >
                    <Nav.Item>
                        <Nav.Link href="/home">Active </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-1">Link </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-2">Link </Nav.Link>
                    </Nav.Item>
                    </Nav>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                        <div className="container">
                        <a className="navbar-brand" href="#">E-Commerce</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="login.html">Login</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Register</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Shop</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Profile</a>
                            </li>
                            </ul>
                        </div>
                        </div>
                    </nav>*/