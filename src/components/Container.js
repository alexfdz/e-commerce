import React from 'react';
import ProfilePage from './ProfilePage';
import LandingPage from './LandingPage';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import InventoryPage from './InventoryPage';

class Container extends React.Component {
    render(){
        return(
        <Router>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                <a className="navbar-brand" href="#">E-Commerce</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to="/" className="nav-link" >Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/Login" className="nav-link" >Login</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/shop" className="nav-link"  >Shop</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/profile" className="nav-link"  >Profile</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/inventory" className="nav-link" >Inventory</Link>
                    </li>
                    </ul>
                </div>
                </div>
            </nav>

            <Switch>
                <Route path="/profile">
                    <ProfilePage/>
                </Route>
                <Route path="/login">

                </Route>
                <Route path="/inventory">
                    <InventoryPage />
                </Route>
                <Route path="/">
                    <LandingPage />
                </Route>
            </Switch>
            </Router>
            );
    }
}
export default Container;