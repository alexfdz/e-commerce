import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function InventoryPage(props) {
    return(
        <div>
            <Container fluid>
            <Row>
                <Col xs={4}>
                    
                    </Col>
                    <Col xs={6}>
                     <h2>Inventory Page</h2> 
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col>
                    <Form>
                        <Form.Row className="align-items-center">
                            <Form.Group as={Col} controlId="1">
                            <Form.Label>Type of product</Form.Label>
                            <Form.Control as="select" defaultValue="Choose...">
                            <option>Choose...</option>
                            <option>...</option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridZip">
                            <Form.Label>Product ID</Form.Label>
                            <Form.Control placeholder="Product"/>
                            </Form.Group>
                            <Form.Group as={Col} controlId="2">
                            <br/>
                            <Button variant="outline-secondary"  >Search</Button>
                            </Form.Group>
                            <Form.Group as={Col} controlId="2">
                            <br/>
                            <Button variant="outline-secondary" >Add product</Button>{' '}
                            <Button variant="outline-secondary" >edit product</Button>
                            </Form.Group>
                        </Form.Row>
                        
                    </Form>
                    </Col>
                </Row>
                <Row>
                    <Col>
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                        <Tab eventKey="home" title="Overview">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        <Tab eventKey="best" title="Best Sellers">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        <Tab eventKey="sale" title="Sale Items">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        <Tab eventKey="low" title="Low Stock Products">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        <Tab eventKey="profile" title="Purchase info">
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Varient</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>shoes</td>
                                    <td>20</td>
                                    <td>19.99</td>
                                    <td>clothes</td>
                                    <td>Red, blue, black</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Tab>
                        </Tabs>
                        </Col>
                </Row>
            </Container>
        </div>
    )

}
export default InventoryPage;
/**/