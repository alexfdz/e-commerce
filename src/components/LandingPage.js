import React from 'react';
import './LandingPage.css';

class LandingPage extends React.Component {
    render(){
    return(
        <div>
            <head>
            <title>Full Width Pics - Start Bootstrap Template</title>
            <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"></link>
            <link href="css/full-width-pics.css" rel="stylesheet"></link>

            </head>
            <body>
            
            <header className="py-5 bg-image-full"></header>
            
            <section className="py-5">
                <div className="container">
                <h1>About us</h1>
                <p className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, suscipit, rerum quos facilis repellat architecto commodi officia atque nemo facere eum non illo voluptatem quae delectus odit vel itaque amet.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, suscipi</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, suscipi</p>
                
                </div>
            </section>

            <footer className="py-5 bg-dark">
                <div className="container">
                <p className="m-0 text-center text-white">Copyright &copy; PK 2020</p>
                </div>

            </footer>

            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            </body>
        </div>
    );
    }
}
export default LandingPage;
/*<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                <a className="navbar-brand" href="#">E-Commerce</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="login.html">Login</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Register</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Shop</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Profile</a>
                    </li>
                    </ul>
                </div>
                </div>
            </nav>*/ 